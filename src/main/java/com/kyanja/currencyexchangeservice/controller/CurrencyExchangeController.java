package com.kyanja.currencyexchangeservice.controller;

import com.kyanja.currencyexchangeservice.dto.ExchangeValueDto;
import com.kyanja.currencyexchangeservice.model.ExchangeValueModel;
import com.kyanja.currencyexchangeservice.service.ExchangeValueService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/currency-exchange-service-api")
public class CurrencyExchangeController {


    @Value("${server.port}")
    private int port;


    private final Environment environment;


    private final ExchangeValueService service;


    private final ModelMapper modelMapper;

    public CurrencyExchangeController(Environment environment, ExchangeValueService service, ModelMapper modelMapper) {
        this.environment = environment;
        this.service = service;
        this.modelMapper = modelMapper;
    }


    @GetMapping("/currency-exchange/from/{from}/to/{to}")       //where {from} and {to} are path variable
    public ExchangeValueDto retrieveExchangeValue(@PathVariable String from, @PathVariable String to)  //from map to USD and to map to INR
    {

        ExchangeValueDto  exchangeValue = new ExchangeValueDto();
        //   return new  ExchangeValue(1000L, from, to, BigDecimal.valueOf(65));
        // taking the exchange value
        //     ExchangeValue exchangeValue= new ExchangeValue (1000L, from, to, BigDecimal.valueOf(65));
        ExchangeValueModel exchangeValueModel= service.findByFromAndTo(from,to);
        //picking port from the environment
        exchangeValueModel.setPort(port);

        exchangeValue = modelMapper.map(exchangeValueModel, ExchangeValueDto.class);

        return exchangeValue;
    }

    public Environment getEnvironment() {
        return environment;
    }



    public ModelMapper getModelMapper() {
        return modelMapper;
    }

//    @Autowired
//    private Environment environment;
//    @Autowired
//    private ExchangeValueRepository repository;
//    @GetMapping("/currency-exchange/from/{from}/to/{to}")       //where {from} and {to} are path variable
//    public ExchangeValueModel retrieveExchangeValue(@PathVariable String from, @PathVariable String to)   //from map to USD and to map to INR
//    {
//        ExchangeValueModel exchangeValue = repository.findByFromAndTo(from, to);
//      //setting the port
//
//        exchangeValue.setPort(port);
//
//
//        return exchangeValue;
//    }
}
