package com.kyanja.currencyexchangeservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.config.environment.Environment;

public class ProfileManager {


    @Autowired
    private Environment environment;

    @Value("${spring.profiles.active}")
    private String activeProfiles;

    public void getActiveProfiles() {
        for (String profileName : environment.getProfiles()) {
            System.out.println("Currently active profile - " + activeProfiles);
        }
    }



}

