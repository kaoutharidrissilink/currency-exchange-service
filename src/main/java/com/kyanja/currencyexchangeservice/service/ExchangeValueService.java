package com.kyanja.currencyexchangeservice.service;

import com.kyanja.currencyexchangeservice.model.ExchangeValueModel;

public interface ExchangeValueService {

    ExchangeValueModel findByFrom (String from);
    ExchangeValueModel findByFromAndTo(String from, String to);
}
