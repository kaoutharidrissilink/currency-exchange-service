package com.kyanja.currencyexchangeservice.service.impl;

import com.kyanja.currencyexchangeservice.dao.ExchangeValueRepository;
import com.kyanja.currencyexchangeservice.model.ExchangeValueModel;
import com.kyanja.currencyexchangeservice.service.ExchangeValueService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ExchangeValueServiceImpl implements ExchangeValueService {


    private final ExchangeValueRepository repository;

    public ExchangeValueServiceImpl(ExchangeValueRepository repository) {
        this.repository = repository;
    }

    @Override
    public ExchangeValueModel findByFrom(String from) {
        return repository.findByFrom(from);
    }

    @Override
    public ExchangeValueModel findByFromAndTo(String from, String to) {
        return repository.findByFromAndTo(from,to);
    }
}
