package com.kyanja.currencyexchangeservice.dao;

import com.kyanja.currencyexchangeservice.model.ExchangeValueModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface ExchangeValueRepository extends JpaRepository<ExchangeValueModel, Long> {

    //creating query method
    ExchangeValueModel findByFromAndTo(String from, String to);

    ExchangeValueModel findByFrom(String from);
}

